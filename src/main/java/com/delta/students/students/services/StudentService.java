package com.delta.students.students.services;

import java.util.List;

import com.delta.students.students.models.Student;

public interface StudentService {

    List<Student> findAll();

    // method to save
    Student save(Student student);

    // method to find an object by id
    Student findById(Long id);

    //method to delete an object from the database
    void delete(Long id);

    
}
