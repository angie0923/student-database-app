package com.delta.students.students.repositories;

import com.delta.students.students.models.Student;

import org.springframework.data.jpa.repository.JpaRepository;;

public interface StudentRepo extends JpaRepository<Student, Long> {
    
}
